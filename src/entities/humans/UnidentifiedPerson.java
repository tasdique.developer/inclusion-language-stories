package entities.humans;

public class UnidentifiedPerson implements Person {
    public String getActionPronoun() {
        return "they";
    }

    public String getReferentialPronoun() {
        return "them";
    }

    public String getPossessivePronoun() {
        return "their";
    }

    public String getPluralPronoun() { return "people"; }
}
